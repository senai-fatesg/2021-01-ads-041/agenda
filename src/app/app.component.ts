import { Component } from '@angular/core';
import { Pessoa } from './aula-typescript';
import { environment } from 'src/environments/environment';


export class Produto{
  constructor(public nome: string, public preco: number, public quantidade: number){

  }

  getTotal(): number{
    return this.preco * this.quantidade;
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'agenda';

  produtos: Produto[] = [];


  produto: Produto = new Produto('', 0, 0);

  add(){
    this.produtos.push(this.produto);
    this.produto = new Produto('', 0, 0);
  }

  getTotalPedido(): number{
    let retorno = 0;
    for(let p of this.produtos){
      retorno += Number(p.getTotal());
    }
    return retorno;
  }

  excluir(p: Produto) {
    let idx = this.produtos.indexOf(p);
    console.log(idx);
    this.produtos.splice(idx, 1);
  }

  editar(p: Produto) {
    this.produto = p;
  }

}
